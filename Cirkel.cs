﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_23._2___Bart
{
    internal class Cirkel
    {
        private double _straal;

        public Cirkel() 
        { 

        }
        public Cirkel(double straal)
        {
            Straal = straal;
        }
        public double Straal
        {
            get { return _straal; }
            set { _straal = value;}
        }
        public double BerekenOmtrek()
        {
            return Math.Round(Straal * 2 * Math.PI, 2);
        }
        public double BerekenOppervlakte()
        {
            return Math.Round(Straal * Straal * Math.PI, 2);
        }
        public string FormattedOmtrek()
        {
            return BerekenOmtrek().ToString();
        }
        public string FormattedOppervlakte()
        {
            return BerekenOppervlakte().ToString();
        }
        public override string ToString()
        {
            return "Omtrek:" + FormattedOmtrek().PadLeft(20) + Environment.NewLine + "Oppervlakte:" + FormattedOppervlakte().PadLeft(15);
        }
    }
}
