﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oefening_23._2___Bart
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnBereken_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cirkel cirkel = new Cirkel(Convert.ToDouble(txtStraal.Text));
                lblInhoud.Content = cirkel.ToString();
            }
            catch 
            {
                MessageBox.Show("De ingegeven waarde moet numeriek zijn!", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}